package gitlab.infra.audit;

import org.springframework.lang.NonNull;

import java.util.Optional;
import java.util.UUID;

public class AuditCallback {

    public static final ThreadLocal<Auditory.Transaction> auditThreadLocal = new ThreadLocal<>();

    public static void generate(@NonNull final String app, @NonNull final String user){
        auditThreadLocal.set(new Auditory.Transaction(UUID.randomUUID().toString(),user, app));
    }
    public static Optional<Auditory.Transaction> get(){
        return Optional.ofNullable(auditThreadLocal.get());
    }

}
