package gitlab.infra.audit;

import lombok.RequiredArgsConstructor;
import javax.annotation.PostConstruct;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.beans.factory.annotation.Value;

@Component
@RequiredArgsConstructor
public class AuditSender {
    private static AuditSender instance;
    private final KafkaTemplate kafka;

    @Value("${audit.sender.topic}")
    private String topic;

    @PostConstruct
    private void load(){
        instance = this;
    }


    public static AuditSender getInstance() {
        return instance;
    }

    public void send(@NonNull final Auditory.Action action, @NonNull final Object entity){
        AuditCallback.get()
                .ifPresent(transaction -> kafka.send(topic, Auditory.builder()
                        .acao(action.getSigla())
                        .dados(entity)
                        .transacao(transaction.id())
                        .usuario(transaction.user())
                        .aplicacao(transaction.app())
                        .entidade(entity.getClass().getName()).build()));
    }

}
