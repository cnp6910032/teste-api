package gitlab.infra.audit;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Auditory {
    private String transacao;
    private String usuario;
    private String aplicacao;
    private String acao;
    private String entidade;
    private Object dados;

    @Getter
    @AllArgsConstructor
    public enum Action{
        SAVE("INSERT"), UPDATE("UPDATE"), DELETE("DELETE"), READ("SELECT");
        private String sigla;
    }

    public record Transaction(String id, String user, String app) {}

}
