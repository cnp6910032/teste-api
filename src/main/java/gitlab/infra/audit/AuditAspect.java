package gitlab.infra.audit;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.boot.info.BuildProperties;
import org.springframework.beans.factory.annotation.Value;


import gitlab.infra.helper.KeycloakHelper;

@Slf4j
@Aspect
@Component
@RequiredArgsConstructor
public class AuditAspect {

    private final KeycloakHelper keycloakHelper;

    @Value("${spring.application.name}")
    private String app;

    @Value("${audit.enable}")
    private boolean auditable;

    @Before("@annotation(Audit)")
    public void before(@NonNull final JoinPoint joinPoint) throws Throwable {
        if(auditable) AuditCallback.generate(app, keycloakHelper.getUserName());
    }

}
