package gitlab.util;

import javax.validation.Validation;
import javax.validation.Validator;
import org.springframework.lang.NonNull;

public class ValidateUtil{
    private static Validator validator = validator = Validation.buildDefaultValidatorFactory().getValidator();

    public static boolean doValidate(@NonNull final Object entity, @NonNull final String field, @NonNull final String validation ){
        return validator.validate(entity).stream().anyMatch(violation ->
                field.equals(violation.getPropertyPath().toString())
                        && validation.equals(violation.getMessageTemplate()));
    }

}
