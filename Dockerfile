FROM openjdk:17-jdk-alpine3.14

# Adicionando o usuario
ARG APPLICATION_USER=appuser

#RUN apk add font-roboto
RUN adduser --no-create-home -u 1000 -D $APPLICATION_USER \
   && mkdir /app \
   && chown -R $APPLICATION_USER /app

USER 1000
COPY --chown=1000:1000 ./target/teste-api.jar /app/app.jar
WORKDIR /app
ENV SPRING_PROFILES_ACTIVE=prod
EXPOSE 8080
ENTRYPOINT [ "java","-Djava.awt.headless=true", "-jar", "app.jar" ]
